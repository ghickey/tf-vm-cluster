
variable "node_count" {
  description = "Number of nodes for cluster"
  type        = string
  default     = "1"
}

variable "name" {
  description = "Name of instance to be created"
  type        = string
}

variable "instance_type" {
  description = "Google instance type"
  type        = string
}

variable "zone" {
  description = "Google zone to create instance"
  type        = string
}

variable "boot_disk_size" {
  description = "Boot disk size in GB"
  type        = string
  default       = "10"
}

variable "boot_disk_image" {
  description = "Image to use for the boot disk"
  type        = string
  default     = "Debian GNU/Linux 9"
  # debian-cloud/debian-9
}

variable "boot_disk_type" {
  description = "Disk type, pd-standard or pd-ssd"
  type        = string
  default     = "pd-standard"
}

variable "delete_disks" {
  description = "Delete disks when instance is deleted"
  type        = string
  default     = "true"
}

variable "scopes" {
  description = "List of scopes to use when creating instance"
  type        = list
  default     = ["userinfo-email", "compute-ro", "storage-ro"]
}

variable "startup_script_filename" {
  description = "Filename of the user data script to configure the master node. (Default: _tpl_docker-user-data.sh)"
  default     = "See the description for the default value."  # Default moved to locals config block.
}

variable "startup_script_substitution_table" {
  description = "A map of patterns(keys) to search for and values to substitute"
  type        = map
  default     = {}
}

variable "pre_exec_filename" {
  description = "The filename containing shell commands to execute before customized user-data code"
  default     = "See the description for the default value."  # Default moved to locals config block.
}

variable "post_exec_filename" {
  description = "The filename containing shell commands to execute after customized user-data code"
  default     = "See the description for the default value."  # Default moved to locals config block.
}

variable "labels" {
  description = "Labels to add to instance"
  type        = map
  default     = {}
}

variable "extra_tags" {
  description = "Additional tags to add to the cluster"
  type        = map
  default     = {}
}

variable "metadata" {
  description = "Metadata to add to instance"
  type        = map
  default     = {}
}

variable "preemptible" {
  description = "Set the preemptible flag for the instance"
  type        = string
  default     = "false"
}

variable "restart" {
  description = "Set the automatic_restart flag on the instance"
  type        = string
  default     = "true"
}



locals {
  default_startup_script_filename = "${path.module}/_tpl_startup_script.sh"
  startup_script_filename         = "${var.startup_script_filename != "See the description for the default value." ? var.startup_script_filename : local.default_startup_script_filename}"

  default_pre_exec_filename  = "${path.module}/_tpl_pre_exec_script.sh"
  pre_exec_filename          = "${var.pre_exec_filename != "See the description for the default value." ? var.pre_exec_filename : local.default_pre_exec_filename}"

  default_post_exec_filename = "${path.module}/_tpl_post_exec_script.sh"
  post_exec_filename         = var.post_exec_filename != "See the description for the default value." ? var.post_exec_filename : local.default_post_exec_filename

  restart = "${var.preemptible == "true" ? "false" : var.restart}"
}
