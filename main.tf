terraform {
  backend "gcs" {}
}

module "cluster" {
  #source = "git::ssh://git@gitlab.com/ghickey/tf-gce-instance.git?ref=multicloud"
  source = "/Users/hickey/gitlab/terraform/tf-gce-instance/work/multicloud"

  driver = var.driver

  name          = "${var.nugget}-${var.name}"
  node_count    = var.node_count
  nugget        = var.nugget
  instance_type = var.instance_type
  zone          = var.zone
  region        = var.region
  project       = var.project
  environment_name = var.environment_name
  department    = var.department
  product_name  = var.product_name
  env_repo      = var.env_repo
  owner         = var.owner


  #labels = module.meta.tags

  metadata = var.metadata

  boot_disk_image = var.boot_disk_image
  boot_disk_size  = var.boot_disk_size
  boot_disk_type  = var.boot_disk_type
  delete_disks    = var.delete_disks

  preemptible = var.preemptible
  restart = var.restart

  scopes = var.scopes

  pre_exec_filename = var.pre_exec_filename
  post_exec_filename = var.post_exec_filename
  startup_script_filename = var.startup_script_filename
  startup_script_substitution_table = var.startup_script_substitution_table

  # env_repo         = var.env_repo
  # environment_name = var.environment_name
  # project          = var.project

  # customer_facing   = "false"
  # department        = var.department
  # owner             = var.owner
  # tf_module         = "ghickey__tf-gce-instance"
  # tf_module_version = "multicloud"

  extra_tags = var.extra_tags
}


